<?php namespace Decoupled\Wordpress\Assets;

use Decoupled\Wordpress\Assets\PathsInterface;

class AssetConfig implements AssetConfigInterface{

    protected $scripts = [];

    protected $paths;

    protected $fs;

    const PUBLIC_FOLDER_NAME = 'public';

    /**
     * Alias for AssetConfig::add method
     *
     * @param      string   $id        The identifier
     * @param      string   $path      The path to script
     * @param      array    $deps      array of script ids that script depends on
     * @param      string   $version   The version of the script
     * @param      boolean  $inFooter  Whether script should be loaded in header or footer
     *
     * @return     Decoupled\Wordpress\Bundle\Asset\AssetConfig   ( self )
     */

    public function __invoke( $id, $path, array $deps = [], $version = false, $inFooter = true )
    {
        return $this->add( $id, $path, $deps, $version, $inFooter );
    }

    /**
     * Will copy contents from given directory into template public directory
     * prefixed by given id: example
     * 
     * if you want to use assets locally from the vendor directory. 
     * 
     * /vendor/package/src/path/to/public/dir
     * 
     * all the contents of the folder will be copied here
     * 
     * /public/{$id}/
     * 
     * all links within asset files should be relative
     *
     * @param      string  $dir    The dir containing the assets
     * @param      string  $id     The id  
     * 
     * @return     Decoupled\Wordpress\Bundle\Asset\AssetConfig   ( self )
     */

    public function uses( $dir, $id )
    {
        if( !is_dir($dir) )
        {
            throw new \Exception('Directory '.$dir.' does not exist');
        }

        $fs = $this->getFileSystem();

        $path = $fs->makeLocalDirname( $id );

        //recursive copy to directory
        $fs->copyFolderContents( $dir, $path );

        //add new path to namespaces collection
        $this->getPathCollection()->add( $id, $path );

        return $this;
    }

    /**
     * Sets the file system object
     *
     * @param      Decoupled\Wordpress\Bundle\Asset\FileSystemInterface  $files  The files
     *
     * @return     Decoupled\Wordpress\Bundle\Asset\AssetConfig   ( self )
     */

    public function setFileSystem( FileSystemInterface $fs )
    {
        $this->fs = $fs;

        return $this;
    }

    /**
     * Gets the file system object
     *
     * @return     Decoupled\Wordpress\Bundle\Asset\FileSystemInterface  The file system.
     */

    public function getFileSystem()
    {
        return $this->fs;
    }

    /**
     * Sets the path collection object
     *
     * @param      Paths   $paths  The paths
     *
     * @return     Decoupled\Wordpress\Bundle\Asset\AssetConfig   ( self )
     */

    public function setPathCollection( PathsInterface $paths )
    {
        $this->paths = $paths;

        return $this;
    }

    /**
     * Gets the path collection object
     *
     * @return     Decoupled\Wordpress\Assets\PathsInterface  The path collection.
     */

    public function getPathCollection()
    {
        return $this->paths;
    }

    /**
     * Registers script to be enqueued later
     *
     * @param      string   $id        The identifier
     * @param      string   $path      The path to script
     * @param      array    $deps      array of script ids that script depends on
     * @param      string   $version   The version of the script
     * @param      boolean  $inFooter  Whether script should be loaded in header or footer
     *
     * @return     Decoupled\Wordpress\Bundle\Asset\AssetConfig   ( self )
     */

    public function add( $id, $path, array $deps = [], $version = false, $inFooter = true )
    {
        $params = func_get_args();

        $this->scripts[] = $id;

        $this->scripts = array_unique($this->scripts);

        $params[1] = $this->getPathCollection()->getFullPath( $path );

        call_user_func_array('wp_register_script', $params);

        return $this;
    }

    /**
     * deregister script by id
     *
     * @param      string  $id     The identifier
     *
     * @return     Decoupled\Wordpress\Bundle\Asset\AssetConfig   ( self )
     */

    public function remove( $id )
    {
        $key = array_search($id, $this->scripts);

        unset( $this->scripts[$key] );

        //reorder array
        $this->scripts = array_values( $this->scripts );

        wp_deregister_script($id);

        return $this;
    }

    /**
     * Gets the registered script ids.
     *
     * @return     array of  The scripts.
     */

    public function getScripts()
    {
        return $this->scripts;
    }

}