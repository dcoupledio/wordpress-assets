<?php namespace Decoupled\Wordpress\Assets;


class FileSystem implements FileSystemInterface{

    const PUBLIC_FOLDER_NAME = 'public';

    /**
     * Gets the public folder name.
     *
     * @return     string  The public folder name.
     */

    public static function getPublicFolderName()
    {
        return static::PUBLIC_FOLDER_NAME;
    }

    /**
     * Makes a local dir, with given namespace
     *
     * @param      string  $id     The namespace of the path
     *
     * @return     string  full path
     */

    public static function makeLocalDirname( $id )
    {
        return get_template_directory() .'/'.self::getPublicFolderName().'/'.$id;
    }

    /**
     * Makes a local Uri path to asset directory
     *
     * @param      string  $id     The path id
     *
     * @return     string  string  the uri
     */

    public static function makeLocalUripath( $id )
    {
        return get_template_directory_uri() .'/'.self::getPublicFolderName().'/'.$id;
    }

    /**
     * copies the contents of given directory to new directory
     * if the new directory does not exist
     *
     * @param      string  $src    The source directory
     * @param      string  $path   The output directory
     * 
     * @return     void
     */

    public static function copyFolderContents( $src, $path )
    {
        if(is_dir($path)) return;

        mkdir( $path, 0755, true );

        $dir = opendir($src); 

        while(false !== ( $file = readdir($dir)) ) { 
            if (( $file != '.' ) && ( $file != '..' )) { 
                if ( is_dir($src . '/' . $file) ) { 
                    recurse_copy($src . '/' . $file,$path . '/' . $file); 
                } 
                else { 
                    copy($src . '/' . $file,$path . '/' . $file); 
                } 
            } 
        } 
        closedir($dir);   
    }

}