<?php namespace Decoupled\Wordpress\Assets;

class AssetQueue implements AssetQueueInterface{

    /**
     * enqueues given scripts
     *
     * @param      string|array|AssetConfigInterface  $scripts  The scripts
     */

    public function add( $scripts )
    {
        $scripts = self::toScriptArray( $scripts );

        array_map(function( $script ){

            wp_enqueue_script( $script );
        }, $scripts);
    }

    /**
     * remove script from queue
     *
     * @param      mixed  $scripts  The scripts to remove from queue
     */

    public function remove( $scripts )
    {
        $scripts = self::toScriptArray( $scripts );

        array_map(function( $script ){

            wp_dequeue_script( $script );
        }, $scripts);
    }

    /**
     * converts script input into array of script names
     *
     * @param      mixed  $scripts  The scripts (Decoupled\Wordpress\Assets\AssetConfigInterface|array|string)
     *
     * @return     array the scripts
     */

    public static function toScriptArray( $scripts )
    {
        if( $scripts instanceof AssetConfigInterface )
        {
            return $scripts->getScripts();
        }

        //then we'll assume a single string was given
        if( !is_array($scripts) )
        {
            return [ (string) $scripts ];
        }

        //if we get here, then it's an array, return it
        return $scripts;
    }
}