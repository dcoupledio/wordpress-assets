<?php namespace Decoupled\Wordpress\Assets;


interface FileSystemInterface{
    
    /**
     * Gets the public folder name.
     *
     * @return     string  The public folder name.
     */

    public static function getPublicFolderName();

    /**
     * Makes a local dir, with given namespace
     *
     * @param      string  $id     The namespace of the path
     *
     * @return     string  full path
     */

    public static function makeLocalDirname( $id );
    /**
     * Makes a local Uri path to asset directory
     *
     * @param      string  $id     The path id
     *
     * @return     string  string  the uri
     */

    public static function makeLocalUripath( $id );

    /**
     * copies the contents of given directory to new directory
     * if the new directory does not exist
     *
     * @param      string  $src    The source directory
     * @param      string  $path   The output directory
     * 
     * @return     void
     */

    public static function copyFolderContents( $src, $path );
}