<?php namespace Decoupled\Wordpress\Assets;

interface PathsInterface{

    /**
     * add path the paths collection
     *
     * @param      string  $namespace  The namespace
     * @param      string  $path       The path
     */

    public function add( $namespace, $path );

    /**
     * removes path from paths collection by namespace
     *
     * @param      string $namespace  The namespace
     */

    public function remove( $namespace );

    /**
     * gets the path by given namespace
     *
     * @param      string  $namespace  The namespace
     */

    public function get( $namespace );

    /**
     * replaces namespaced path with full path to given file/dir
     *
     * @param      string  $path   The namespaced path
     */

    public function getFullPath( $path );
}