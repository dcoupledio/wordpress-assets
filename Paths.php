<?php namespace Decoupled\Wordpress\Assets;


class Paths implements PathsInterface{

    const NAMESPACE_PREFIX = '@';

    /**
     * collection of paths
     *
     * @var        array
     */

    protected $paths = [];

    /**
     * add path the paths collection
     *
     * @param      string  $namespace  The namespace
     * @param      string  $path       The path
     * 
     * @return     Decoupled\Wordpress\Assets\Paths (self) 
     */

    public function add( $namespace, $path )
    {
        $this->paths[$namespace] = $path;

        return $this;
    }

    /**
     * removes path from paths collection by namespace
     *
     * @param      string $namespace  The namespace
     * 
     * @return     Decoupled\Wordpress\Assets\Paths (self) 
     */

    public function remove( $namespace )
    {
        unset($this->paths[$namespace]);

        return $this;
    }

    /**
     * gets the path by given namespace
     *
     * @param      string  $namespace  The namespace
     * 
     * @return     string  the path
     */

    public function get( $namespace )
    {
        return $this->paths[$namespace];
    }


    /**
     * replaces namespaced path with full path to given file/dir
     *
     * @param      string  $path   The namespaced path
     * 
     * @return     string  full path
     */

    public function getFullPath( $path )
    {
        $path = trim($path);

        if( 0 !== strpos($path, static::NAMESPACE_PREFIX) ) return $path;

        $matches = [];

        preg_match('/.+?(?=\/)/', $path, $matches);

        $namespace = substr( $matches[0], 1 );

        return str_replace('@'.$namespace, $this->get($namespace), $path);

    }    
}