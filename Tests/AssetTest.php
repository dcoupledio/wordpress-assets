<?php

require('../vendor/autoload.php');

require('./mock.php');

use phpunit\framework\TestCase;
use Decoupled\Wordpress\Assets\AssetConfig;
use Decoupled\Wordpress\Assets\AssetQueue;
use Decoupled\Wordpress\Assets\Paths;
use Decoupled\Wordpress\Assets\FileSystem;

class AssetTest extends TestCase{

    public function testCanRegister()
    {
        $assets = new AssetConfig();

        $assets->setPathCollection( new Paths() );

        $assets->setFileSystem( new FileSystem() );

        $assets->add( 'jquery', 'path/to/jquery.js' );

        $this->assertContains( 'jquery', WP_Mock::$regScripts );

        $this->assertEquals( count( $assets->getScripts() ), 1 );

        $assets->remove( 'jquery' );

        $this->assertEmpty( WP_Mock::$regScripts );

        return $assets;
    }

    /**
     * @depends testCanRegister
     */

    public function testCanEnqueScripts( $assets )
    {
        $queue = new AssetQueue();

        $assets->add( 'bootstrap', 'path/to/bootstrap.css' );

        $queue->add( $assets );

        $queue->add( 'test' );

        $this->assertContains( 'bootstrap', WP_Mock::$enqScripts );

        $this->assertContains( 'test', WP_Mock::$enqScripts );

        $assets->add( 'test', 'path/to/test.js' );

        $queue->remove( $assets );

        $this->assertEmpty( WP_Mock::$enqScripts );

        return $assets;
    }

    /**
     * @depends testCanEnqueScripts
     */

    public function testCanWriteLocalAsset( $assets )
    {
        $queue = new AssetQueue();

        $assets->uses( dirname(__FILE__).'/assets', 'bundle.namespace' );

        $assets->add( 'asset.js', '@bundle.namespace/asset.js' );

        $this->assertTrue( file_exists( dirname(__FILE__, 2).'/public/bundle.namespace/asset.js' ) );

        return $assets;
    }

    /**
     * @depends testCanWriteLocalAsset
     */

    public function testCanGetLocalPaths( $assets )
    {
        $paths = $assets->getPathCollection();

        $dir   = $paths->get( 'bundle.namespace' );

        $file  = $paths->getFullPath( '@bundle.namespace/js/file.js' );

        $this->assertTrue( file_exists($dir) );

        $this->assertEquals( dirname(__FILE__, 2).'/public/bundle.namespace/js/file.js', $file );
    }

}