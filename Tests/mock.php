<?php

class WP_Mock{
    
    public static $regScripts = [];

    public static $enqScripts = [];
}


function wp_register_script( $id )
{
    WP_Mock::$regScripts[$id] = $id;
}

function wp_deregister_script( $id )
{
    unset( WP_Mock::$regScripts[$id] );
}

function wp_enqueue_script( $id )
{
    WP_Mock::$enqScripts[$id] = $id;
}

function wp_dequeue_script( $id )
{
    unset( WP_Mock::$enqScripts[$id] );
}

function get_template_directory()
{
    return dirname(__FILE__, 2);
}

function get_template_directory_uri()
{
    return 'http://wordpress.com';
}
