<?php namespace Decoupled\Wordpress\Assets;

interface AssetConfigInterface{

    /**
     * Alias for AssetConfig::add method
     *
     * @param      string   $id        The identifier
     * @param      string   $path      The path to script
     * @param      array    $deps      array of script ids that script depends on
     * @param      string   $version   The version of the script
     * @param      boolean  $inFooter  Whether script should be loaded in header or footer
     */

    public function __invoke( $id, $path, array $deps = [], $version = false, $inFooter = true );


    /**
     * Registers script to be enqueued later
     *
     * @param      string   $id        The identifier
     * @param      string   $path      The path to script
     * @param      array    $deps      array of script ids that script depends on
     * @param      string   $version   The version of the script
     * @param      boolean  $inFooter  Whether script should be loaded in header or footer
     */

    public function add( $id, $path, array $deps = [], $version = false, $inFooter = true );

    /**
     * deregister script by id
     *
     * @param      string  $id     The identifier
     */

    public function remove( $id );


    /**
     * Gets the registered script ids.
     *
     * @return     array of  The scripts.
     */

    public function getScripts();

}