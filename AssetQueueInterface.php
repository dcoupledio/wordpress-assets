<?php namespace Decoupled\Wordpress\Assets;

interface AssetQueueInterface{

    /**
     * enqueues given scripts
     *
     * @param      string|array|AssetConfigInterface  $scripts  The scripts
     */

    public function add( $scripts );

    /**
     * remove script from queue
     *
     * @param      mixed  $scripts  The scripts to remove from queue
     */

    public function remove( $scripts );
}